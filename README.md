`Draft`

**FOR IMMEDIATE RELEASE: AUGUST XX, 2019**
```
Sten Pittet
Tability Inc
+61 409 222 883
sten@tability.io
```

## Tability is building the first accountability platform for teams
Tability, a remote startup split between Sydney and Portland, is building the first accountability platform focused on Product teams. Previously at Atlassian, the 2 co-founders Sten Pittet and Bryan Schuldt left the company to solve a simple problem: how to keep teams focused on what matters the most.

Tability CEO Sten Pittet said: "Every company has a plan. But it's hard to keep it in mind when you have dozens of meetings and countless emails every week. Tability reminds everyone of their top priorities and makes it super easy to share progress."

More than 500 companies signed up for Tability since its launch, using it to keep track of their goals, OKRs and projects. "Our customers love the flexibility and simplicity — keeping things lean helps them focus on execution and creates fast feedback loops on outcomes", says Bryan Schuldt.

--

**About Tability**

Tability is an accountability platform for teams. More than 500 teams have signed up for Tability since its launch in January 2019, ranging from organizations of less than 10 people to companies having thousands of employees.

